var cascadingSelectConfig
var parentSelect
var childSelect

function loadCascading (parent, child, url) {
  parentSelect = document.getElementById(parent)
  childSelect = document.getElementById(child)
  cascadingSelectConfig = getSetup(url)
  cascadingSelectConfig.then((data) => initialize(data, parentSelect, childSelect))
}

const getSetup = async(url) => {
  const json = await fetch(url).then((resp) => resp.json())
  return json
}

var initialize = function (data, parent, child) {
  setupParentSelect(parent, child)
  var defaultParent = createNode('option')
  setupParentNode(defaultParent, data['default'], child)
  append(parent, defaultParent)
  updateChild(data['default'].children, child)
  for (var i = 0; i < Object.keys(data).length - 1; i++) {
    var element = createNode('option')
    setupParentNode(element, data[i], child)
    append(parent, element)
  }
}

function setupParentSelect (select, childSelect) {
  select.setAttribute('onchange', 'updateChildSelect()')
}

function updateChildSelect () {
  emptySelect(childSelect)
  const value = parentSelect.value
  cascadingSelectConfig.then((data) => {
    for (var key in data) {
      if (data[key].value === value) {
        updateChild(data[key].children, childSelect)
        return
      }
    }
  })
}

function setupParentNode (element, parent, childSelect) {
  element.innerHTML = parent.text
  element.value = parent.value
}

function emptySelect (select) {
  for (var i = select.options.length - 1; i >= 0; i--) {
    select.remove(i)
  }
}

var updateChild = function (children, childSelect) {
  for (var i = 0; i < children.length; i++) {
    var child = createNode('option')
    setupChildNode(child, children[i].text, children[i].value)
    append(childSelect, child)
  }
}

function setupChildNode (element, innerHTML, value) {
  element.innerHTML = innerHTML
  element.value = value
}

function createNode (element) {
  return document.createElement(element)
}

function append (parent, element) {
  return parent.appendChild(element)
}
